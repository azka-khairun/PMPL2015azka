from django.test import TestCase
from arithmetic import *
from django.core.exceptions import ValidationError

class TestConversion(TestCase):    
    def testDectoBinPositive(self):
        self.assertEqual('000000000000110100', binary(52))
    def testDectoBinZero(self):
        self.assertEqual('000000000000000000',binary(0))
    def testDectoBinNegativ(self):
        self.assertEqual('111111111111101000',binary(-24))
    def binary(self):
        self.assertFail()    
